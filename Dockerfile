FROM openjdk:8-alpine
ADD /build/libs/cicd-demo-0.1.0.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]