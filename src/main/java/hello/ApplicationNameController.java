package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApplicationNameController {

    Logger logger = LoggerFactory.getLogger(ApplicationNameController.class);

    @RequestMapping("/applicationName")
    public ApplicationName applicationName(@RequestParam String name) {
        logger.info("api hit");
        return new ApplicationName(name);
    }
}
