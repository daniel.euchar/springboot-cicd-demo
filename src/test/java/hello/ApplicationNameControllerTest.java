package hello;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ApplicationNameControllerTest {


    @Test
    public void shouldReturnApplicationName(){
        String name = "BookApplication";
        ApplicationNameController applicationNameController = new ApplicationNameController();
        ApplicationName applicationName = applicationNameController.applicationName(name);
        assertEquals(name,applicationName.getName());
    }


}
